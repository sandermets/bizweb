import { Ng2FrontPage } from './app.po';

describe('ng2-front App', function() {
  let page: Ng2FrontPage;

  beforeEach(() => {
    page = new Ng2FrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
