import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AuthGuard } from './shared/auth-guard';

import { CookieService } from 'angular2-cookie/services/cookies.service';

import { TranslateModule } from 'ng2-translate';

import {
  BizService,
  DealerService,
  DepartmentService,
  DepbizService,
  AuthenticationService
} from './dao/index';

import { HomeComponent } from './features/home/home.component';
import { BizComponent } from './features/biz/biz.component';
import { DealerComponent } from './features/dealer/dealer.component';
import { DepartmentComponent } from './features/department/department.component';
import { DepbizComponent } from './features/depbiz/depbiz.component';
import { LoginComponent } from './features/login/login.component';

import { AlertModule } from 'ng2-bootstrap';
import { HeaderMenuComponent } from './shared/header-menu/header-menu.component';
import { ArrayOrderByPipe } from './shared/pipes/array-order-by.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BizComponent,
    DealerComponent,
    DepartmentComponent,
    DepbizComponent,
    LoginComponent,
    HeaderMenuComponent,
    ArrayOrderByPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    AlertModule.forRoot(),
    TranslateModule.forRoot()
  ],
  providers: [
    CookieService,
    AuthenticationService,
    AuthGuard,
    BizService,
    DealerService,
    DepartmentService,
    DepbizService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
