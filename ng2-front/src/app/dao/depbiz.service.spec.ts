/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DepbizService } from './depbiz.service';

describe('DepbizService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepbizService]
    });
  });

  it('should ...', inject([DepbizService], (service: DepbizService) => {
    expect(service).toBeTruthy();
  }));
});
