import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class AuthenticationService {

  constructor(
    private http: Http,
    private cookieService: CookieService
  ) { }

  login(username: string, password: string) {

    let url: any = '/api/login';
    let headers: any = new Headers();

    headers.append("Authorization", "Basic " + btoa(username + ":" + password));
    headers.append("Content-Type", "application/json");

    return this.http
      .post(url, {}, { headers: headers })
      .map((response: Response) => {
        let result = response.json();
        if (result && result.success && result.token) {
          this.cookieService.removeAll();
          this.cookieService.put('tokenfront', result.token, { expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000) });
          localStorage.setItem('currentUser', JSON.stringify(result));
          return result;
        } else {
          return null;
        }
      });
  }

  logout() {
    this.cookieService.removeAll();
    localStorage.removeItem('currentUser');
  }

}
