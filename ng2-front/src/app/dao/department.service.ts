import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Department } from './entity/department';

@Injectable()
export class DepartmentService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('/api/department' + '?' + (+(new Date)), this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(department: Department) {
    return this.http
      .post('/api/department', department, this.jwt())
      .map((response: Response) => response.json());
  }

  update(department: Department) {
    return this.http
      .put('/api/department/' + department._id, department, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('/api/department/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
