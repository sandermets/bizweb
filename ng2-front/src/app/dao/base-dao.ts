import { Headers, RequestOptions } from '@angular/http';

export class BaseDao {

    constructor() { }

    jwt() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers(
                { 'x-access-token': currentUser.token }
            );
            return new RequestOptions({ headers: headers });
        }
    }


}
