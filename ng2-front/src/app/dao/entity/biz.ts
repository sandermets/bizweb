export class Biz {
  _id: string;
  name: string;
  slug: string;
  type: string;
  notes: string;
}
