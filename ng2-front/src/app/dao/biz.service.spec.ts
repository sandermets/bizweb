/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BizService } from './biz.service';

describe('BizService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BizService]
    });
  });

  it('should ...', inject([BizService], (service: BizService) => {
    expect(service).toBeTruthy();
  }));
});
