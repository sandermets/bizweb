import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Dealer } from './entity/dealer';

@Injectable()
export class DealerService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('/api/dealer' + '?' + (+(new Date)), this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(dealer: Dealer) {
    return this.http
      .post('/api/dealer', dealer, this.jwt())
      .map((response: Response) => response.json());
  }

  update(dealer: Dealer) {
    return this.http
      .put('/api/dealer/' + dealer._id, dealer, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('/api/dealer/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

  getView() {
    return this.http
      .get('/api/dealer-view' + '?' + (+(new Date)), this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }
}
