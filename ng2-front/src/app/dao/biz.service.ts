import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Biz } from './entity/biz';

@Injectable()
export class BizService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  create(name: string, file: File, type: string, notes: any) {
    let input = new FormData();
    input.append("file", file);
    input.append("name", name);
    input.append("type", type);
    if (!notes) {
      notes = '';
    }
    input.append("notes", notes);

    return this.http
      .post('/api/biz', input, this.jwt())
      .map((response: Response) => response.json());
  }

  getList() {
    return this.http
      .get('/api/biz' + '?' + (+(new Date)), this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  delete(_id: string) {
    return this.http
      .delete('/api/biz/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
