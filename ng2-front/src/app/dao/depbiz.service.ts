import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Depbiz } from './entity/depbiz';

@Injectable()
export class DepbizService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('/api/depbiz' + '?' + (+(new Date)), this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(depbiz: Depbiz) {
    return this.http
      .post('/api/depbiz', depbiz, this.jwt())
      .map((response: Response) => response.json());
  }

  update(depbiz: Depbiz) {
    return this.http
      .put('/api/depbiz/' + depbiz._id, depbiz, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('/api/depbiz/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
