import { Component, OnInit } from '@angular/core';

import { Department } from '../../dao/entity/department';
import { Dealer } from '../../dao/entity/dealer';

import { DepartmentService } from '../../dao/department.service';
import { DealerService } from '../../dao/dealer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {

  model: Department = new Department();
  models: Department[] = [];
  dealers: Dealer[] = [];
  selectedDealer: Dealer = new Dealer;
  loading: boolean = false;

  private getModels() {
    this.modelService.getList().subscribe(
      result => {
        this.models = result.data;
        this.getDealers();
      },
      error => {
        if (+error.status === 403) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

  private insertModel() {
    this.modelService
      .create(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private updateModel() {
    this.modelService
      .update(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private deleteModel(_id: string) {
    this.modelService
      .delete(_id)
      .subscribe(result => {
        if (result.success) {
          this.getModels();
        }
      });
  }

  private getDealers() {
    this.dealerService
      .getList()
      .subscribe(result => {
        this.dealers = result.data;
        if (this.model._id) {
          this.selectDealerById(this.model.dealer._id);
        }
      });
  }

  constructor(
    private modelService: DepartmentService,
    private dealerService: DealerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getModels();
  }

  resetModel() {
    this.model = new Department;
    this.selectedDealer = new Dealer();
  }

  upsertModel() {
    this.model._id ? this.updateModel() : this.insertModel();
  }

  editModel(model: Department) {
    this.resetModel();
    this.model = model;
    this.selectDealerById(this.model.dealer._id);
  }

  removeModel(_id: string) {
    this.deleteModel(_id);
  }

  selectDealerById(_id: string) {
    this.selectedDealer = this.dealers.find(d => {
      return d._id === _id
    });
  }

  onSelectedDealerChange() {
    this.model.dealer = this.selectedDealer;
  }

}
