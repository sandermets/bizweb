import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { AuthenticationService } from '../../dao/authentication.service';
import { User } from '../../dao/entity/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: User = new User;
  loading = false;
  returnUrl: string;
  loginError: boolean = false;

  storedPrefLang: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.storedPrefLang = localStorage.getItem('prefLanguage');
  }

  login() {
    this.loginError = false;
    this.loading = true;
    this.authenticationService
      .login(this.model.username, this.model.password)
      .subscribe(
      data => {
        this.router.navigate([this.returnUrl] || ['/']);
      },
      error => {
        this.loading = false;
        this.loginError = true;
      });
  }

  changeLanguage(language: string) {
    localStorage.setItem('prefLanguage', language);
    window.location.href = "/";//hard refresh
  }

}
