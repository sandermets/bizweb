/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';

import { routing } from '../../app.routing';

import { HomeComponent } from '../home/home.component';
import { DepartmentComponent } from '../department/department.component';
import { DepbizComponent } from '../depbiz/depbiz.component';
import { LoginComponent } from '../login/login.component';
import { DealerComponent } from '../dealer/dealer.component';

import { BizComponent } from './biz.component';

describe('BizComponent', () => {
  let component: BizComponent;
  let fixture: ComponentFixture<BizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderMenuComponent,
        HomeComponent,
        BizComponent,
        DealerComponent,
        DepartmentComponent,
        DepbizComponent,
        LoginComponent
      ],
      imports: [
        routing,
        FormsModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
