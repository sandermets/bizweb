import { Component, OnInit } from '@angular/core';
import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';

import { Biz } from '../../dao/entity/biz';
import { BizService } from '../../dao/biz.service';
import { TranslateService } from 'ng2-translate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-biz',
  templateUrl: './biz.component.html',
  styleUrls: ['./biz.component.scss']
})
export class BizComponent implements OnInit {

  errorMsg: any = { code: '', message: '' };
  showSuccess: boolean = false;
  loadingInProgress: boolean = false;
  showError: boolean = false;
  model: Biz = new Biz();
  models: Biz[] = [];
  file: File;

  uploadTypes: any = [
    { value: 'web', display: 'Model' },
    { value: 'file', display: 'File' }
  ];

  constructor(
    private router: Router,
    private modelService: BizService,
    private translate: TranslateService
  ) { }

  private startAction() {
    this.showError = false;
    this.showSuccess = false;
    this.loadingInProgress = true;
  }

  handleActionResult(result: any) {
    if (!result.success) {
      this.showError = true;
      this.errorMsg.code = result.code;
      this.translate.get('SHARED.' + result.code).subscribe(
        (res: string) => {
          this.errorMsg.message = res;
        });
    } else {
      this.showSuccess = true;
    }
  }

  private getModels() {

    this.modelService.getList().subscribe(
      result => {
        if (result.success) {
          this.models = result.data;
        }
      },
      error => {
        if (+error.status === 403) {
          this.router.navigate(['/login']);
        }
      },
      () => { }
    );

  }

  private insertModel() {
    if (this.model.name && this.file) {
      this.startAction();
      this.modelService
        .create(this.model.name, this.file, this.model.type, this.model.notes)
        .subscribe(result => {
          this.handleActionResult(result);
          if (result.success) {
            this.resetModel();
            this.getModels();
          }

        },
        error => { },
        () => {
          this.loadingInProgress = false;
        });
    }
  }

  private deleteModel(_id: string) {

    this.startAction();

    this.modelService.delete(_id).subscribe(
      result => {
        this.handleActionResult(result);

        if (result.success) {
          this.getModels();
        }
      },
      error => { },
      () => {
        this.loadingInProgress = false;
      });
  }

  ngOnInit() {
    this.getModels();
  }

  resetModel() {
    this.model = new Biz();
    this.file = null;
  }

  addModel(fileupload: any, uType: any) {
    this.model.type = uType.value;
    if (fileupload.files.length) {
      this.file = fileupload.files[0];
      this.insertModel();
    }
  }

  removeModel(_id: string) {
    this.deleteModel(_id);
  }

  formatDate(dateString: string) {
    let d = new Date(dateString);
    let dateArray = [d.getDate(), (d.getMonth() + 1), d.getFullYear()];

    return localStorage.getItem('prefLanguage') === 'en' ?
      dateArray.join('/') :
      dateArray.join('.');
  }

}
