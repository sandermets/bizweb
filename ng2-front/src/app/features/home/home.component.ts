import { Component, OnInit } from '@angular/core';

import { Dealer } from '../../dao/entity/dealer';
import { DealerService } from '../../dao/dealer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isClassVisibleObject: any = {};

  isClassVisibleInnerObject: any = {};

  model: Dealer = new Dealer();
  models: Dealer[] = [];

  constructor(
    private modelService: DealerService,
    private router: Router
  ) { }

  setToggleVar(i: string) {
    if (!this.isClassVisibleObject.hasOwnProperty(i)) {
      this.isClassVisibleObject[i] = true;
    } else {
      this.isClassVisibleObject[i] = !this.isClassVisibleObject[i];
    }
  }

  setToggleVarInner(ij: string) {
    if (!this.isClassVisibleInnerObject.hasOwnProperty(ij)) {
      this.isClassVisibleInnerObject[ij] = true;
    } else {
      this.isClassVisibleInnerObject[ij] = !this.isClassVisibleInnerObject[ij];
    }
  }

  ngOnInit() {
    this.loadModels();
  }

  loadModels() {
    this.modelService.getView().subscribe(
      result => {
        if (result.success) {
          this.models = result.data;
        }
      },
      error => {
        if(+error.status === 403) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

}
