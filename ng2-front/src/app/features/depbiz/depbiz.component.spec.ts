/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { routing } from '../../app.routing';

import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';
import { HomeComponent } from '../home/home.component';
import { BizComponent } from '../biz/biz.component';
import { DepartmentComponent } from '../department/department.component';
import { LoginComponent } from '../login/login.component';
import { DealerComponent } from '../dealer/dealer.component';

import { DepbizComponent } from './depbiz.component';

describe('DepbizComponent', () => {
  let component: DepbizComponent;
  let fixture: ComponentFixture<DepbizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        BizComponent,
        DealerComponent,
        DepartmentComponent,
        DepbizComponent,
        LoginComponent,
        HeaderMenuComponent
      ],
      imports: [
        routing,
        FormsModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepbizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
