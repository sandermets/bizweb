import { Component, OnInit } from '@angular/core';

import { Depbiz } from '../../dao/entity/depbiz';
import { Department } from '../../dao/entity/department';
import { Biz } from '../../dao/entity/biz';

import { DepbizService } from '../../dao/depbiz.service';
import { DepartmentService } from '../../dao/department.service';
import { BizService } from '../../dao/biz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-depbiz',
  templateUrl: './depbiz.component.html',
  styleUrls: ['./depbiz.component.scss']
})
export class DepbizComponent implements OnInit {

  model: Depbiz = new Depbiz;
  models: Depbiz[] = [];

  department: Department = new Department;
  departments: Department[] = [];

  biz: Biz = new Biz;
  bizs: Biz[] = [];

  selectedDepartment: Department = new Department;
  selectedBiz: Biz = new Biz;

  private getDepartments() {
    this.departmentService.getList().subscribe(
      result => {
        this.departments = result.data;
        if (this.model._id) {
          this.selectedDepartment = this.departments.find(d => d._id === this.model.department._id);
        }
      });
  }

  private getBizs() {
    this.bizService
      .getList()
      .subscribe(result => {
        this.bizs = result.data;
        if (this.model._id) {
          this.selectedBiz = this.bizs.find(d => d._id === this.model.department._id);
        }
      });
  }

  private getModels() {
    this.modelService.getList().subscribe(
      result => {
        this.models = result.data;
        this.getDepartments();
        this.getBizs();
      },
      error => {
        if (+error.status === 403) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

  private insertModel() {
    this.modelService
      .create(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private updateModel() {
    this.modelService
      .update(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private deleteModel(_id: string) {
    this.modelService
      .delete(_id)
      .subscribe(result => {
        if (result.success) {
          this.getModels();
        }
      });
  }

  constructor(
    private modelService: DepbizService,
    private bizService: BizService,
    private departmentService: DepartmentService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getModels();
  }

  removeModel(_id: string) {
    this.deleteModel(_id);
  }

  upsertModel() {
    if (this.model.biz && this.model.department) {
      this.model._id ? this.updateModel() : this.insertModel();
    }
  }

  selectDepartmentById(_id: string) {
    this.selectedDepartment = this.departments.find(d => {
      return d._id === _id
    });
  }

  selectBizById(_id: string) {
    this.selectedBiz = this.bizs.find(d => {
      return d._id === _id
    });
  }

  resetModel() {
    this.model = new Depbiz;
    this.selectedDepartment = new Department;
    this.selectedBiz = new Biz;
  }

  editModel(model: Depbiz) {
    this.resetModel();
    this.model = model;
    this.selectDepartmentById(this.model.department._id);
    this.selectBizById(this.model.biz._id);
  }

  onSelectedDepartmentChange() {
    this.model.department = this.selectedDepartment;
  }

  onSelectedBizChange() {
    this.model.biz = this.selectedBiz;
  }

}
