import { Component, OnInit } from '@angular/core';
import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';

import { Dealer } from '../../dao/entity/dealer';
import { DealerService } from '../../dao/dealer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.scss']
})
export class DealerComponent implements OnInit {

  model: Dealer = new Dealer();
  models: Dealer[] = [];

  private getModels() {
    this.modelService.getList().subscribe(
      result => {
        this.models = result.data;
      }, error => {
        if (+error.status === 403) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

  private insertModel() {
    this.modelService
      .create(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private updateModel() {
    this.modelService
      .update(this.model)
      .subscribe(result => {
        this.resetModel();
        this.getModels();
      });
  }

  private deleteModel(_id: string) {
    this.modelService
      .delete(_id)
      .subscribe(result => {
        if (result.success) {
          this.getModels();
        }
      });
  }

  constructor(
    private modelService: DealerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getModels();
  }

  resetModel() {
    this.model = new Dealer();
  }

  upsertModel() {
    this.model._id ? this.updateModel() : this.insertModel();
  }

  editModel(model: Dealer, f: any) {//f actually gives for reg. try to bump up angular
    this.model = model;
  }

  removeModel(_id: string) {
    //TODO CONFIRM TO REMOVE Maybe ask user password?
    this.deleteModel(_id);
  }

}
