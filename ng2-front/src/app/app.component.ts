import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'BizWeb';

  constructor(translate: TranslateService) {
    let defaultLang = 'et';
    let prefLang = localStorage.getItem('prefLanguage');

    if (!prefLang) {
      prefLang = defaultLang;
    }
    translate.setDefaultLang(prefLang);
    translate.use(prefLang);
    localStorage.setItem('prefLanguage', prefLang);
  }

}