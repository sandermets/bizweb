import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayOrderBy'/*,
  pure: false //greedy activates for every change */
})
export class ArrayOrderByPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === Object(value) && value.length) {
      return value.sort((a, b) => {
        if (a[args] < b[args]) {
          return -1;
        }
        if (a[args] > b[args]) {
          return 1;
        }
        return 0;
      });
    } else {
      return value;
    }
  }

}
