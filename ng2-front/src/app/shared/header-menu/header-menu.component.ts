import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit {
  isManager: boolean = false;
  isIn = false;   // store state


  constructor() { }

  ngOnInit() {
    let userData = JSON.parse(localStorage.getItem('currentUser'));
    this.isManager = userData.role === 'manager' ? true : false;
  }
  toggleState() {
    let bool = this.isIn;
    this.isIn = bool === false ? true : false;
  }

}
