import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './shared/auth-guard';

import { HomeComponent } from './features/home/home.component';
import { BizComponent } from './features/biz/biz.component';
import { DealerComponent } from './features/dealer/dealer.component';
import { DepartmentComponent } from './features/department/department.component';
import { DepbizComponent } from './features/depbiz/depbiz.component';
import { LoginComponent } from './features/login/login.component';

const appRoutes: Routes = [

    //Public
    { path: 'login', component: LoginComponent },

    //Logged In
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },

    //Role manager
    { path: 'dealer', component: DealerComponent, canActivate: [AuthGuard] },
    { path: 'department', component: DepartmentComponent, canActivate: [AuthGuard] },
    { path: 'upload', component: BizComponent, canActivate: [AuthGuard] },
    { path: 'models', component: DepbizComponent, canActivate: [AuthGuard] },

    //otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);