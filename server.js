const env = process.env.NODE_ENV || "development";
const port = process.env.PORT || 3333;
const mongo_uri = process.env.MONGO_URI || "mongodb://localhost:27017/bizweb";

const express = require('express');// Get the packages we need
const app = express();// Create our Express application
const mongoose = require('mongoose');
const bodyParser = require('body-parser');//accept data via POST or PUT
const Promise = require('bluebird');
const routerDefined = require('./routers/routes');
const authController = require('./controllers/auth');
const cookieParser = require('cookie-parser');
const passport = require('passport');

mongoose.Promise = require('bluebird');//http://mongoosejs.com/docs/promises.html
const db = mongoose.connect(mongo_uri);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());

/* //with back end proxy, provided by angular -cli seems irrelevant at the moment
// Add headers - http://stackoverflow.com/questions/18310394/no-access-control-allow-origin-node-apache-port-issue
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,x-access-token');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});
*/

app.use('/', express.static(__dirname + '/ng2-front/dist'));
app.use('/api', routerDefined);
app.use('/m', authController.hasCookieJWT, express.static(__dirname + '/public'));
app.use('/i18n', authController.publicAccess, express.static(__dirname + '/i18n'));
app.get('*', function (req, res) {
  res.redirect('/');
});
app.listen(port);

console.log(`Successfully started! Listening on :${port}`);
//Enabling Graceful Shutdown - http://pm2.keymetrics.io/docs/usage/docker-pm2-nodejs/
process.on('SIGINT', () => {
  db.disconnect(err => {
    if (err) {
      console.log(`Error closing db connection ${err.message}`);
      process.exit(1);
    } else {
      console.log(`Database successfully stopped!`);
      process.exit(0);
    }
  });
});
