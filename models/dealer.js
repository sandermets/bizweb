const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const DealerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    departments: [{ type: Schema.Types.ObjectId, ref: 'Department' }]//Dealer has Many Departments 
  }, {
    timestamps: true
  }
);

const DealerModel = mongoose.model('Dealer', DealerSchema);
module.exports = DealerModel;

module.exports.newDealer = function () {
  return new DealerModel();
};


