const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const DepartmentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    dealer: {//Department hasOne Dealer
      type: Schema.Types.ObjectId, ref: 'Dealer',
      required: true
    },
    depbizs: [//Department hasMany DepBiz
      { type: Schema.Types.ObjectId, ref: 'DepBiz' }
    ]
  }, {
    timestamps: true
  }
);

const DepartmentModel = mongoose.model('Department', DepartmentSchema);

module.exports = DepartmentModel;

module.exports.newDepartment = function () {
  return new DepartmentModel();
};


