const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');
const bcrypt = require('bcrypt-nodejs');

const UserSchema = new mongoose.Schema({// Define our user schema
  username: {
    type: String,
    unique: true,
    required: true
  },
  role: {
    type: String,
    enum: ['reader', 'manager'],
    default: 'reader'
  },
  password: {
    type: String,
    required: true
  }
}, {
    timestamps: true
  }
);

UserSchema.pre('save', function (callback) {// Execute before each user.save() call
  let user = this;

  if (!user.isModified('password')) {// Break out if the password hasn't changed
    return callback();
  }
  //TODO it re modify password
  console.log('modifing password');
  bcrypt.genSalt(5, (err, salt) => {// Password changed so we need to hash it
    if (err) {
      return callback(err);
    }

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) {
        return callback(err);
      }
      user.password = hash;
      callback();
    });

  });

});

UserSchema.methods.verifyPassword = function (password, cb) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    console.log('Inside veriFy password');
    console.log(password);
    console.log(this.password);
    if (err) {
      console.log('bcrypt.compare err', err);
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);


