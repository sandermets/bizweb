const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const DepBizSchema = new mongoose.Schema(
  {
    department: {//depbiz hasOne Department
      type: Schema.Types.ObjectId, ref: 'Department',
      required: true
    },
    biz: {//depbiz hasOne BizModel
      type: Schema.Types.ObjectId, ref: 'Biz',
      required: true
    },
    notes: {
      type: String
    },
    ordernumber: {
      type: Number
    }
  }, {
    timestamps: true
  }
);

const DepBizModel = mongoose.model('DepBiz', DepBizSchema);

module.exports = DepBizModel;

module.exports.newDepBiz = function () {
  return new DepBizModel();
};


