const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const BizSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      required: true
    },
    type: {
      type: String,
      default: 'web'
    },
    notes: {
      type: String
    },
    depbizs: [//Biz hasMany DepBiz
      { type: Schema.Types.ObjectId, ref: 'DepBiz' }
    ]
  }, {
    timestamps: true
  }
);

const BizModel = mongoose.model('Biz', BizSchema);

module.exports = BizModel;

module.exports.newBizModel = function () {
  return new BizModel();
};


