# BizWeb

## Purpose

This app, expressjs + ng2, is used to serve html5 projects.  

User in role manager

* can upload zipped directory containing index.html and possible assets  
* can create dealers, departments and relate html5 projects to departments
  
User in role reader

* can browse presented structure 
* can view html5 projects as webpages associated to departments 

## Node 6

    npm i -g yarn

### Local Install

#### Development

    yarn && cd ./ng2-front && yarn && ./node_modules/angular-cli/bin/ng build

#### Production

    yarn && cd ./ng2-front && yarn && ./node_modules/angular-cli/bin/ng build -prod && cd ..

## Run BE

    node server.js

## Run Front

    cd ng2-front && ./node_modules/angular-cli/bin/ng serve --proxy-config proxy.conf.json

## Other

[Local Docker](./wiki/LOCAL.md)  
[Server Docker](./wiki/PRODUCTION.md)  
[Front](./ng2-front/README.md)  
