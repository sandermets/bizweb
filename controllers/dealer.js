const Promise = require('bluebird');
const Dealer = require('../models/dealer');
const Department = require('../models/department');
const DepBiz = require('../models/depbiz');
const Biz = require('../models/biz');

/**
 * @api {post} /api/dealer
 */
exports.postDealer = function (req, res) {

  if (!req.body.name) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }

  const dealer = Dealer.newDealer();

  dealer.name = req.body.name;

  dealer.save()
    .then(() => {
      res.json({ success: true, data: dealer, code: 201 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {get} /api/dealer
 */
exports.getDealers = function (req, res) {
  Dealer.find()
    .then(dealers => {
      res.json({ success: true, data: dealers });
    })
    .catch(err => {
      res.json({ success: false });
      console.log(err.message);
    });
};

/**
 * @api {get} /api/dealer/:dealerId
 */
exports.getDealer = function (req, res) {
  Dealer.findById(req.params.dealerId)
    .then(dealer => {
      res.json({ success: true, data: dealer });
    })
    .catch(err => {
      res.json({ success: false });
      console.log(err.message);
    });
};

/**
 * @api {put}  /api/dealer/:dealerId
 */
exports.putDealer = function (req, res) {

  if (!req.body.name) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }

  Dealer
    .findById(req.params.dealerId)
    .then(dealerFound => {
      if (dealerFound) {
        dealerFound.name = req.body.name;
        return dealerFound.save();
      }
    })
    .then(dealerUpdated => {
      if (dealerUpdated) {
        return res.json({ success: true, code: 202, data: dealerUpdated });
      }
      res.json({
        success: false, code: 404
      });
    })
    .catch(err => {
      res.json({ success: false });
      console.log(err.message);
    });
};

/**
 * @api {delete} /api/dealer/:dealerId
 */
exports.deleteDealer = function (req, res) {
  Dealer
    .findById(req.params.dealerId)
    .then(dealer => {
      if (!dealer) {
        return res.json({ success: false, code: 404 });
      }
      if (dealer.departments.length) {
        return res.json({ success: false, code: 409 });
      }
      dealer
        .remove()
        .then(() => {
          res.json({ success: true, code: 202 });
        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {get} /api/dealer-view
 */
exports.getDealersView = function (req, res) {

  let populateQuery = {
    path: 'departments',
    model: 'Department',
    select: 'name depbizs',
    populate: {
      path: 'depbizs',
      model: 'DepBiz',
      select: 'biz notes ordernumber',
      populate: {
        path: 'biz',
        model: 'Biz',
        select: 'name slug'
      }
    }
  };

  Dealer
    .find()
    .select('name departments')
    .populate(populateQuery)
    .then(models => {
      if (models) {
        return res.json({ success: true, data: models, code: 200 });
      }
      res.json({ success: true, data: [], code: 204 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};
