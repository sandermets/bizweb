const User = require('../models/user');
const _ = require('lodash');

/**
 * @api {post} /api/user
 */
exports.postUser = function (req, res) {

  if (!req.body.username || !req.body.password || !req.body.role) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }

  const user = new User({
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  });

  user.save()
    .then(() => {
      res.json({ success: true, code: 201 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.warn(err.message);
    });
};

/**
 * @api {post} /api/user
 */
exports.getUsers = function (req, res) {
  User.find()
    .then(users => {
      res.json({ success: true, code: 200, data: _.map(users, user => _.pick(user, ['username', 'role'])) });
    })
    .catch(err => {
      res.json({ code: 500, success: false });
      console.warn(err.message);
    });
};
