const Promise = require('bluebird');
const bizModelService = require('../services/bizModelService');
const BizModel = require('../models/biz');
const _ = require('lodash');

/**
 * @api {post} /api/biz
 */
exports.postModelUpload = function (req, res) {

    bizModelService
      .addModel(req, res)
      .spread((success, data) => {
        if (success) {
          res.json({ success: true, code: 201, data: data });
        }
      })
      .catch(err => {
        res.json({ success: false, data: err, code: 500 });
        console.warn(err);
      });
};

/**
 * @api {get} /api/biz
 */
exports.getModelUploads = function (req, res) {
  BizModel
    .find()
    .then(models => {
      if (models) {
        return res.json({ success: true, data: models, code: 200 });
      }
      res.json({ success: true, data: [], code: 204 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {delete} /biz/:bizId
 */
exports.deleteModel = function (req, res) {
  bizModelService
    .deleteModel(req, res)
    .then(result => {
      if (result) {
        res.json({ success: true, code: 202 });
      }
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.warn(err);
    });
};
