const Promise = require('bluebird');
const DepBiz = require('../models/depbiz');
const Department = require('../models/department');
const Biz = require('../models/biz');

/**
 * @api {post} /api/depbiz
 */
exports.postDepBiz = function (req, res) {
  if (!req.body.department || !req.body.biz) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }
  const depbiz = DepBiz.newDepBiz();
  depbiz.department = req.body.department._id;
  depbiz.biz = req.body.biz._id;
  
  depbiz.notes = req.body.notes ? req.body.notes : '';
  depbiz.ordernumber = req.body.ordernumber ? req.body.ordernumber : 0;
  depbiz
    .save()
    .then(model => {
      Promise
        .all([
          Department.findById(req.body.department._id).then(department => {
            department.depbizs.push(model);
            return department.save();
          }),
          Biz.findById(req.body.biz._id).then(biz => {
            biz.depbizs.push(model);
            return biz.save();
          })
        ])
        .then(() => {
          res.json({ success: true, data: model, code: 201 });
        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {get} /api/depbiz
 */
exports.getDepBiz = function (req, res) {
  let populateQuery = [
    {
      path: 'department',
      model: 'Department',
      select: 'name dealer',
      populate: {
        path: 'dealer',
        model: 'Dealer',
        select: 'name'
      }
    }, {
      path: 'biz',
      model: 'Biz',
      select: 'name slug'
    }
  ];
  DepBiz
    .find()
    .select('department biz notes ordernumber')
    .populate(populateQuery)
    .then(models => {
      if (models) {
        return res.json({ success: true, data: models, code: 200 });
      }
      res.json({ success: true, data: [], code: 204 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {delete} /api/depbiz/:depbizId
 */
exports.deleteDepBiz = function (req, res) {
  const id = req.params.depbizId;
  DepBiz
    .findById(id)
    .then(model => {
      if (!model) {
        return res.json({ success: false, code: 404 });
      }
      console.log(model.department)
      Promise
        .all([
          Department.findById(model.department).then(department => {
            department.depbizs.pull(model);
            return department.save();
          }),
          Biz.findById(model.biz).then(biz => {
            biz.depbizs.pull(model);
            return biz.save();
          })
        ])
        .then(() => {
          model
            .remove()
            .then(() => {
              res.json({ success: true, code: 202 });
            })
            .catch(err => {
              res.json({ success: false, code: 500 });
              console.log(err.message);
            });
        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};


exports.putDepBiz = function (req, res) {
  if (!req.body.department || !req.body.biz) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }
  const id = req.params.depbizId;

  DepBiz
    .findById(id)
    .then(model => {
      if (!model) {
        return res.json({ success: false, code: 404 });
      }
      console.log(model.department)
      Promise
        .all([
          Department.findById(model.department).then(department => {
            department.depbizs.pull(model);
            return department.save();
          }),
          Biz.findById(model.biz).then(biz => {
            biz.depbizs.pull(model);
            return biz.save();
          })
        ])
        .then(() => {

          model.department = req.body.department._id;
          model.biz = req.body.biz._id;
          model.notes = req.body.notes ? req.body.notes : '';
          model.ordernumber = req.body.ordernumber ? req.body.ordernumber : 0;

          model
            .save()
            .then(modelUpdated => {
              //debbiz updated add refs to associated models
              Promise
                .all([
                  Department.findById(req.body.department._id)
                    .then(department => {
                      department.depbizs.push(modelUpdated);
                      return department.save();
                    }),
                  Biz.findById(req.body.biz._id)
                    .then(biz => {
                      biz.depbizs.push(modelUpdated);
                      return biz.save();
                    })
                ])
                .then(() => {
                  res.json({ success: true, data: modelUpdated, code: 201 });
                })
                .catch(err => {
                  res.json({ success: false, code: 500 });
                  console.log(err.message);
                });
            })
            .catch(err => {
              res.json({ success: false, code: 500 });
              console.log(err.message);
            });

        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
}
