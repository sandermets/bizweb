const jwt_secret = process.env.JWT_SECRET || '123456';

const expiresInSeconds = 180;
//const expiresInSeconds = process.env.JWT_EXPIRES_IN_SECONDS || 60 * 60 * 24;

//const expiresInDate = new Date(Date.now() + expiresInSeconds * 1000);

const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const User = require('../models/user');
const jwt = require('jsonwebtoken');

passport.use(new BasicStrategy(
  function (username, password, callback) {
    console.log('Start auth');
    console.log(username, password);
    User.findOne({ username: username }, function (err, user) {
      if (err) {
        console.log('User.findOne err', err);
        console.log(err);
        return callback(err);
      }
      if (!user) {// No user found with that username
        console.log('no user');
        return callback(null, false);
      }
      user.verifyPassword(password, function (err, isMatch) {// Make sure the password is correct
        if (err) {
          console.log('verifyPassword err', err);
          console.log(err);
          return callback(err);
        }
        if (!isMatch) {// Password did not match
          console.log('// Password did not match');
          return callback(null, false);
        }

        let token = jwt.sign(
          { _id: user._id, role: user.role },
          jwt_secret,
          { expiresIn: '24h' }
        );

        user.token = token;
        return callback(null, user);// Success
      });
    });
  }
));

exports.isAuthenticated = passport.authenticate('basic', { session: false });

exports.logIn = function (req, res) {
  res.json({ success: true, token: req.user.token, role: req.user.role });
};

exports.hasJWT = function (req, res, next) {
  var token = req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, jwt_secret, function (err, decoded) {// verifies secret and checks exp
      if (err) {
        console.error(err.message);
        return res.status(403).json({
          success: false,
          message: 'hasJWT Failed to authenticate token.'
        });
      } else {
        req.decoded = decoded;// if everything is good, save to request for use in other routes
        next();
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'hasJWT No token provided.'
    });
  }
};

exports.hasCookieJWT = function (req, res, next) {
  //var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var token = req.cookies.tokenfront;
  if (token) {
    jwt.verify(token, jwt_secret, function (err, decoded) {// verifies secret and checks exp
      if (err) {
        console.error(err.message);
        return res.status(403).json({
          success: false,
          message: 'hasCookieJWT Failed to authenticate token.'
        });
      } else {
        req.decoded = decoded;// if everything is good, save to request for use in other routes
        next();
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'hasCookieJWT No token provided.'
    });
  }
};

exports.isManager = function (req, res, next) {
  //var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var token = req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, jwt_secret, function (err, decoded) {// verifies secret and checks exp
      if (err) {
        return res.status(403).json({
          success: false,
          message: 'Failed to authenticate token.'
        });
      } else {
        if (decoded.role === 'manager') {
          req.decoded = decoded;
          next();
        } else {
          return res.status(403).send({
            success: false,
            message: 'ACL'
          });
        }
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'isManager No token provided.'
    });
  }
};

exports.publicAccess = function (req, res, next) {
  next();
};
