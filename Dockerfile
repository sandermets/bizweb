#node image from docker hub
FROM mhart/alpine-node:6
MAINTAINER Sander Mets <sandermets0@gmail.com>
LABEL Name=bizweb Version=1.0.0
RUN npm i pm2 -g
RUN npm i yarn -g
COPY package.json /tmp/package.json
RUN cd /tmp && npm i unicode && yarn
RUN mkdir -p /usr/src/app && mv /tmp/node_modules /usr/src
WORKDIR /usr/src/app
COPY . /usr/src/app
EXPOSE 80
CMD pm2-docker process.prod.yml --env production

