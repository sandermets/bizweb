# Local server

## Directories directories for persistant data

### Dev

    mkdir -p ~/appdata/local/uploads
    mkdir -p ~/appdata/local/public
    mkdir -p ~/appdata/local/db

### Production alike for bu test

    mkdir -p ~/appdata/production/uploads
    mkdir -p ~/appdata/production/public
    mkdir -p ~/appdata/production/db

## Build DB

    cd mongodb/
    chmod +x docker-entrypoint.sh && \
        docker build -t bizweb-db:1.0.0 .

## Run DB

NB! be sure that port 27017 is NOT exposed to Internet in host, use nmap or similar tool.

    docker run \
        --name bizweb-db \
        -v ~/appdata/production/db:/data/db \
        -d bizweb-db:1.0.0

## Conf process.prod.yml

    cp _process.prod.yml process.prod.yml

Set configuration params

## Build app

    docker build -t bizweb-app:1.1.0 .

## Run app

    docker run \
        --name bizweb-app \
        -p 80:80 \
        -v ~/appdata/production/uploads:/usr/src/app/uploads \
        -v ~/appdata/production/public:/usr/src/app/public \
        --link bizweb-db:mongo \
        -d bizweb-app:1.1.0
