const THE_VERSION = '0.0.10';
const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');
const userController = require('../controllers/user');
const dealerController = require('../controllers/dealer');
const departmentController = require('../controllers/department');
const BizController = require('../controllers/biz');
const depBizController = require('../controllers/depbiz');

//Public
router.get('/', (req, res) => {
  res.json({ message: THE_VERSION, data: {} });
});

//Basic AUTH
router.route('/login')
  .post(authController.isAuthenticated, authController.logIn);

router.route('/user')
  .post(authController.publicAccess, userController.postUser)
  .get(authController.publicAccess, userController.getUsers);

//Dealer
router.route('/dealer')
  .post(authController.isManager, dealerController.postDealer)
  .get(authController.hasJWT, dealerController.getDealers);

router.route('/dealer/:dealerId')
  .get(authController.hasJWT, dealerController.getDealer)
  .put(authController.isManager, dealerController.putDealer)
  .delete(authController.isManager, dealerController.deleteDealer);

router.route('/dealer-view')
  .get(authController.hasJWT, dealerController.getDealersView);

//Department
router.route('/department')
  .post(authController.isManager, departmentController.postDepartment)
  .get(authController.hasJWT, departmentController.getDepartments);

router.route('/department/:departmentId')
  .put(authController.isManager, departmentController.putDepartment)
  .get(authController.hasJWT, departmentController.getDepartment)
  .delete(authController.isManager, departmentController.deleteDepartment);

//Biz
router.route('/biz')
  .post(authController.isManager, BizController.postModelUpload)
  .get(authController.hasJWT, BizController.getModelUploads);

router.route('/biz/:bizId')
  .delete(authController.isManager, BizController.deleteModel);

//DepBiz
router.route('/depbiz')
  .post(authController.isManager, depBizController.postDepBiz)
  .get(authController.hasJWT, depBizController.getDepBiz);

router.route('/depbiz/:depbizId')
  .put(authController.isManager, depBizController.putDepBiz)
  .delete(authController.isManager, depBizController.deleteDepBiz);

module.exports = router;
