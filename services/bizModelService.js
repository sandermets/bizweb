const Promise = require('bluebird');
const formidable = require('formidable');
const path = require('path');
const fs = Promise.promisifyAll(require('fs-extra'));
const extract = require('extract-zip');
const extractAsync = Promise.promisify(extract);
const slugIt = require('slug');

const BizModel = require('../models/biz');
const DepBiz = require('../models/depbiz');

const uploadDir = path.join(__dirname, '..', 'uploads');
const extractDir = path.join(__dirname, '..', 'public');
const opts = { multiples: false, uploadDir: uploadDir };

function deleteByPath(pathToRemove) {
  return fs.removeAsync(pathToRemove)
    .then(() => [true, null])
    .catch(err => {
      console.warn(err);
      return [false, err];
    });
};

function parseForm(req) {
  return new Promise((resolve, reject) => {
    let form = new formidable.IncomingForm(opts);//bad NOT TESTABLE move this line to extra file
    form.parse(req, (err, fields, files) => {
      if (err) {
        return reject(err);
      }
      resolve({ fields: fields, files: files });
    });
  });
};

function addTypeModel(req, res, formObj, files, fileRef) {
  let fileObject, fileNameParts, dirSlug, unzipDir;

  const validateUpload = function () {

    fileObject = files[fileRef[0]];
    fileNameParts = fileObject.name.split('.');

    if (fileNameParts[fileNameParts.length - 1] !== 'zip') {//incorrect extension
      res.json({ success: false, msg: 'FILE_INCORRECT_EXTENSION', code: 400, data: {} });
      deleteByPath(fileObject.path);
      return [false];
    }

    /*
    if (fileObject.type !== 'application/zip') {//incorrect mime type
      res.json({ success: false, msg: 'FILE_INCORRECT_TYPE', code: 400, data: {} });
      deleteByPath(fileObject.path);
      return [false];
    }
    */

    return [true];
  };

  const checkIfNew = function (success) {
    if (!success) {
      return [false];
    }

    dirSlug = slugIt(formObj.fields.name).toLowerCase();
    unzipDir = path.join(extractDir, dirSlug);

    return fs
      .readdirAsync(unzipDir)
      .then(result => {
        if (result.length === 1) {//we have a dir move it up to one step
          res.json({ success: false, msg: 'CHANGE_NAME_FIELD', code: 400, data: {} });
          deleteByPath(fileObject.path);
          return [false];
        }
        return [true];//is new, slug is ok?
      })
      .catch(() => {
        return [true];//seems ok to fail here, did not exist
      });
  };

  const extractBizModel = function (success) {
    if (!success) {
      return [false];
    }

    return extractAsync
      .call(extract, fileObject.path, { dir: unzipDir })
      .then(() => {
        deleteByPath(fileObject.path);
        return fs
          .readdirAsync(unzipDir)
          .then(result => {
            if (result.length === 1) {//we have only single result it can be a file or  a directory
              let cpPath = path.join(unzipDir, result[0]);
              return fs.lstatAsync(cpPath)
                .then(stats => {
                  if (stats.isDirectory()) {
                    return fs
                      .copyAsync(cpPath, unzipDir, { clobber: true })
                      .then(() => {
                        deleteByPath(cpPath);
                        return [true];
                      })
                      .catch(err => {
                        console.warn(err);
                        res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
                        deleteByPath(unzipDir);
                        return [false];
                      });
                  }
                  return [true];
                })
                .catch(err => {
                  console.warn(err);
                  res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
                  deleteByPath(unzipDir);
                  return [false];
                });
            }
            return [true];//can't be directory multiple contents
          })
          .catch(err => {
            console.warn(err);
            res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
            deleteByPath(unzipDir);
            return [false];
          });
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_EXTRACTING', code: 400, data: {} });
        console.warn(err);
        deleteByPath(fileObject.path);
        deleteByPath(unzipDir);//?
        return [false];
      });
  };

  const checkIndexExists = function (success) {
    if (!success) {
      return [false];
    }
    return fs
      .readdirAsync(unzipDir)
      .then(result => {
        if (!result || !result.length) {
          deleteByPath(unzipDir);
          return [false];
        }
        let indexExists = false;
        result.forEach(el => {
          if (/index.html/i.test(el)) {
            indexExists = true;
            return;
          }
        });
        if (!indexExists) {
          res.json({ success: false, msg: 'NO_INDEX', code: 400, data: {} });
          deleteByPath(unzipDir);
        }
        return [indexExists];
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_EXTRACTING', code: 400, data: {} });
        console.warn(err);
        deleteByPath(fileObject.path);
        return [false, 500];
      });
  };

  const saveToDb = function (success) {
    if (!success) {
      return [false];
    }

    const bizModel = BizModel.newBizModel();

    bizModel.name = formObj.fields.name;
    bizModel.slug = dirSlug;
    bizModel.notes = formObj.fields.notes;

    return bizModel
      .save()
      .then(savedBizModel => {
        return [true, savedBizModel];
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_SAVING_DB', code: 400, data: {} });
        console.warn(err.message);
        deleteByPath(unzipDir);
      });
  };

  return Promise
    .resolve(validateUpload())
    .spread(checkIfNew)
    .spread(extractBizModel)
    .spread(checkIndexExists)
    .spread(saveToDb)
    .catch(err => {
      res.json({ success: false, msg: 'INTERNAL_ERROR', code: 500, data: {} });
      console.warn(err);
      deleteByPath(fileObject.path);
      return [false, 500];
    });
}

function addTypeFile(req, res, formObj, files, fileRef) {

  let fileObject, fileNameParts, dirSlug, targetPath, fExtension, fSlug;

  const checkIfNew = function () {

    dirSlug = slugIt(formObj.fields.name).toLowerCase();
    fileObject = files[fileRef[0]];
    fExtension = fileObject.name.split('.').slice(-1)[0];
    targetPath = path.join(extractDir, dirSlug) + '.' + fExtension;
    fSlug = dirSlug + '.' + fExtension;
    return fs
      .readdirAsync(targetPath)
      .then(result => {
        if (result.length === 1) {//we have a dir move it up to one step
          res.json({ success: false, msg: 'CHANGE_NAME_FIELD', code: 400, data: {} });
          deleteByPath(fileObject.path);
          return [false];
        }
        return [true];//is new, slug is ok?
      })
      .catch(() => {
        return [true];//seems ok to fail here, did not exist
      });
  };

  //get file info copy or move using slug and file extension

  const copyBizFile = function (success) {
    if (!success) {
      return [false];
    }
    return fs
      .copyAsync(fileObject.path, targetPath, { clobber: true })
      .then(() => {
        deleteByPath(fileObject.path);
        return [true];
      })
      .catch(err => {
        console.warn(err);
        res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
        deleteByPath(fileObject.path);
        deleteByPath(targetPath);
        return [false];
      });
  };

  const saveToDb = function (success) {
    if (!success) {
      return [false];
    }
    const bizModel = BizModel.newBizModel();
    bizModel.name = formObj.fields.name;
    bizModel.slug = fSlug;
    bizModel.notes = formObj.fields.notes;
    return bizModel
      .save()
      .then(savedBizModel => {
        return [true, savedBizModel];
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_SAVING_DB', code: 400, data: {} });
        console.warn(err.message);
        deleteByPath(unzipDir);
      });
  };

  return checkIfNew()
    .spread(copyBizFile)
    .spread(saveToDb)
    .catch(err => {
      res.json({ success: false, msg: 'INTERNAL_ERROR', code: 500, data: {} });
      console.warn(err);
      deleteByPath(fileObject.path);
      return [false, 500];
    });
}

exports.addModel = function (req, res) {

  return parseForm(req)
    .then(formObj => {

      let files = formObj.files;
      let fileRef = Object.keys(formObj.files);

      if (!fileRef.length) {//no file
        res.json({ success: false, msg: 'FILE_MISSING', code: 400, data: {} });
        return [false];
      }

      if (!formObj.fields.name) {
        res.json({ success: false, msg: 'NAME_MISSING', code: 400, data: {} });
        deleteByPath(fileObject.path);
        return [false];
      }

      if (!formObj.fields.type) {
        res.json({ success: false, msg: 'TYPE_MISSING', code: 400, data: {} });
        deleteByPath(fileObject.path);
        return [false];
      }
      if (formObj.fields.type === 'web') {
        return addTypeModel(req, res, formObj, files, fileRef);
      }
      return addTypeFile(req, res, formObj, files, fileRef);
    })
    .catch(err => {
      res.json({ success: false, msg: 'INTERNAL_ERROR', code: 500, data: {} });
      console.error(err);
      if (fileObject) {
        deleteByPath(fileObject.path);
      }
      return [false, 500];
    });
}

exports.deleteModel = function (req, res) {
  const id = req.params.bizId;
  return DepBiz.findOne({ 'biz': id }, 'biz')
    .then(depBizFound => {
      if (depBizFound) {
        res.json({ success: false, code: 409 });
        return [false, 409];
      }
      return [BizModel.findById(id)];
    })
    .spread((bizModelFound, code) => {
      if (!bizModelFound && !code) {
        res.json({ success: false, code: 404 });
        return [false];
      } else if (!bizModelFound && code) {//res already sent
        return [false];
      }
      return [
        bizModelFound,
        fs.removeAsync(path.join(extractDir, bizModelFound.slug))
      ];
    })
    .spread(bizModelFound => {
      if (bizModelFound) {
        return bizModelFound.remove();
      }
      return false;
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
      return false;
    });
};
