# DB Mongo

Mongo image from Docker hub 

## Build

    chmod +x docker-entrypoint.sh && \
    docker build -t bizweb-db:1.0.0 .

## Run

    docker run \
        --name bizweb-db \
        -p 27017:27017 \
        -v /Users/dev/data/bizweb/db:/data/db \
        bizweb-db:1.0.0

## Run as daemon

    docker run \
        --name bizweb-db \
        -p 27017:27017 \
        -v /opt/appdata/production/db:/data/db \
        -d bizweb-db:1.0.0
