## Additional

### docker

    https://github.com/phusion/passenger-docker  
    http://phusion.github.io/baseimage-docker/

### Tutorials

### Back

#### Auth Basic + JWT

    http://scottksmith.com/blog/2014/05/29/beer-locker-building-a-restful-api-with-node-passport/
    https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens


#####  Protect static files with JWT

    http://stackoverflow.com/questions/21335868/how-to-protect-static-folder-in-express-with-passport
    http://stackoverflow.com/questions/33477495/how-to-protect-static-folder-in-express-with-jwt

### Front

    https://angular.io/docs/ts/latest/guide/setup.html
    https://www.tutorialspoint.com/angular2/angular2_environment.htm

    https://www.eyesoreinc.com/getting-started-angular2-using-docker-compose/
    https://docs.docker.com/compose/install/

    https://embed.plnkr.co/?show=preview

    http://jasonwatmore.com/post/2016/09/29/angular-2-user-registration-and-login-example-tutorial

    https://github.com/cornflourblue/angular2-registration-login-example

### Cruft
   Don't use nodemon without exluded upload catagory

  "debug": "node_modules/node-inspector/bin/node-debug.js server.js",
  "start": "node_modules/nodemon/bin/nodemon.js server.js"


Users for development environment

{
    "_id" : ObjectId("585d025d7e3465098c21eb42"),
    "username" : "manager",
    "password" : "$2a$05$HsX6wRe7La9fMzhOkYLatuAtiBlTRHONuqykQkQJ4aOKRqEaqSU8m",
    "role" : "manager",
    "__v" : 0
}

{
    "_id" : ObjectId("585d02697e3465098c21eb43"),
    "username" : "reader",
    "password" : "$2a$05$rJ9KFSfQQ6s5jtYQYx2aduNW3npOIf7m64.Pb0dmNeAaIqmnvoAxi",
    "role" : "reader",
    "__v" : 0
}

Node app for presenting process models made by (Bizagi)[http://www.bizagi.com]  

https://scotch.io/tutorials/simple-language-translation-in-angular-2-part-1

### Digital Ocean

    https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04
    https://www.digitalocean.com/community/tutorials/how-to-use-the-mongodb-one-click-application
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

    http://linoxide.com/linux-how-to/install-couchdb-futon-ubuntu-1604/
    http://pm2.keymetrics.io/docs/usage/quick-start/

### Architectural points to consider

    https://github.com/ngrx/store  
    http://blog.thoughtram.io/angular/2016/02/22/angular-2-change-detection-explained.html#smarter-change-detection  
    https://angular.io/docs/ts/latest/guide/style-guide.html  
    https://medium.com/google-developer-experts/angular-2-introduction-to-redux-1cf18af27e6e#.re95dctrf  
    https://www.packtpub.com/web-development/switching-angular-2  
    http://blog.mgechev.com/2016/04/10/scalable-javascript-single-page-app-angular2-application-architecture/  
    http://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/  

### Run 

PORT=80 node_modules/pm2/bin/pm2 start ecosystem.config.js 