const assert = require('chai').assert;
const Promise = require('bluebird');
const simple = require('simple-mock');

const department = require('../../controllers/department');
const Department = require('../../models/department');
const Dealer = require('../../models/dealer');

describe('Controller department.postDepartment', function () {

  afterEach(function () {
    simple.restore();
  });

  it('should return success true if creating new department is successful', done => {

    let mDepartmentSave = simple.mock(Department, 'newDepartment', () => {
      return {
        save() {
          return Promise.resolve();
        }
      };
    });

    let mDealerFindOne = simple.mock(Dealer, 'findOne', () => {
      return Promise.resolve({
        save() { 
          return Promise.resolve();
        },
        departments: {
          push() { }
        }
      });
    });

    let req = { body: { name: 'dummy-name', dealer: 'dummy-dealer' }, user: { _id: 'dummy-id' } };
    let res = {
      json(inObj) {
        assert.equal(inObj.success, true);
        assert.equal(mDepartmentSave.called, 1);
        done();
      }
    };
    department.postDepartment(req, res);
  });

  it('should return 400 if invalid request payload', done => {
    let req = { body: {} };
    let res = {
      status(httpCode) {
        assert.equal(httpCode, 400);
        return {
          json(inObj) {
            assert.equal(inObj.message, 'Invalid');
            done();
          }
        };
      }
    };
    department.postDepartment(req, res);
  });

  it('should return success false if creating new fails internally', done => {
    let mDepartmentSave = simple.mock(Department, 'newDepartment', () => {
      return {
        save() {
          return Promise.reject(new Error('log error'));
        }
      };
    });
    let req = { body: { name: 'dummy-name', dealer: 'dummy-dealer' }, user: { _id: 'dummy-id' } };
    let res = {
      json(inObj) {
        assert.equal(inObj.success, false);
        assert.equal(mDepartmentSave.called, 1);
        done();
      }
    };
    department.postDepartment(req, res);
  });

});